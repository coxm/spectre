#include <cstdlib>
#include <type_traits>

#include <catch2/catch.hpp>

#include <ogl/ownership/Unique.h>
#include <ogl/Buffer.h>
#include <ogl/Texture.h>

#include "spectre/Batch.h"


struct Sprite {
  int x;
  int y;
  bool visible;
};


struct SpriteFilter {
  inline bool operator()(Sprite const& sprite) const noexcept {
    return sprite.visible;
  }
};


using buffer_type = ogl::Buffer<ogl::ownership::Unique>;
using texture_type = ogl::Texture<ogl::ownership::Unique>;


using filtered_batch_type = spectre::Batch<
  Sprite, SpriteFilter, buffer_type, texture_type>;
using unfiltered_batch_type = spectre::Batch<
  Sprite, spectre::UnfilteredTag, buffer_type, texture_type>;


// Static assertions.
static_assert(filtered_batch_type::s_filteringEnabled);
static_assert(!unfiltered_batch_type::s_filteringEnabled);


constexpr auto TEXTURE_UNIT = GL_TEXTURE13;


template <typename Batch>
void runAssignTests(Batch& batch, std::size_t spriteCount) {
  THEN("size() is correct") {
    CHECK(batch.size() == spriteCount);
  }

  if constexpr(Batch::s_filteringEnabled) {
    THEN("numVisible() is not updated before update() calls") {
      auto const visible = batch.numVisible();
      CHECK(visible == 0);
    }
  }
  else {
    THEN("numVisible() returns the size()") {
      auto const visible = batch.numVisible();
      CHECK(visible == spriteCount);
    }
  }

  THEN("the batch is not empty") {
    CHECK(!batch.empty());
  }

  REQUIRE(spriteCount > 5);

  WHEN("some sprites are marked invisible") {
    batch.at(0).visible = false;
    batch.at(5).visible = false;

    if constexpr(Batch::s_filteringEnabled) {
      THEN("numVisible() is still not updated before update() is called") {
        CHECK(batch.numVisible() == 0);
      }
    }
    else {
      THEN("numVisible() still returns the size") {
        CHECK(batch.numVisible() == spriteCount);
      }
    }

    WHEN("we update() the batch") {
      batch.update();

      if constexpr(Batch::s_filteringEnabled) {
        THEN("numVisible() excludes the invisible sprites") {
          CHECK(batch.numVisible() == spriteCount - 2);
        }
      }
      else {
        THEN("numVisible() still returns the size") {
          CHECK(batch.numVisible() == spriteCount);
        }
      }
    }
  }
}


SCENARIO("Creating a Batch", "[spectre::Batch][gui]") {
  auto testBatch{[](
    std::string const& desc,
    auto& batch,
    std::size_t spriteCount
  ) {
    GIVEN(desc) {
      THEN("the texture unit is as given") {
        CHECK(batch.textureUnit() == TEXTURE_UNIT);
      }
      THEN("size() is zero") {
        CHECK(batch.size() == 0);
      }
      THEN("numVisible() is zero") {
        CHECK(batch.numVisible() == 0);
      }
      THEN("data() is nullptr") {
        CHECK(batch.data() == nullptr);
      }
      THEN("begin() == end()") {
        CHECK(batch.begin() == batch.end());
        CHECK(batch.cbegin() == batch.cend());
      }

      WHEN("we set the texture unit") {
        batch.setTextureUnit(GL_TEXTURE14);
        THEN("the returned texture unit is correct") {
          CHECK(batch.textureUnit() == GL_TEXTURE14);
        }
      }

      WHEN("assigning by copying a value") {
        batch.assign(spriteCount, Sprite{0, 1, true});
        runAssignTests(batch, spriteCount);
      }

      WHEN("assigning from an initializer_list") {
        batch.assign({
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
          Sprite{0, 1, true},
        });
        runAssignTests(batch, 8);
      }

      WHEN("assigning from an iterator") {
        // Use a vector to test genericity.
        std::vector<Sprite> sprites{spriteCount, Sprite{0, 1, true}};
        batch.assign(sprites.cbegin(), sprites.cend());
        runAssignTests(batch, spriteCount);
      }

      WHEN("the batch has sprites assigned") {
        batch.assign(4, Sprite());
        THEN("it is nonempty") {
          CHECK(!batch.empty());
        }

        THEN("begin() != end()") {
          CHECK(batch.begin() != batch.end());
          CHECK(batch.cbegin() != batch.cend());
        }

        THEN("data() != nullptr") {
          CHECK(batch.data() != nullptr);
        }

        WHEN("the batch is cleared") {
          batch.clear();
          THEN("the batch is empty") {
            CHECK(batch.empty());
          }

          THEN("begin() == end()") {
            CHECK(batch.begin() == batch.end());
            CHECK(batch.cbegin() == batch.cend());
          }

          THEN("data() == nullptr") {
            CHECK(batch.data() == nullptr);
          }
        }

        WHEN("we move-construct the batch") {
          auto newBatch(std::move(batch));
          THEN("the new batch holds the data") {
            CHECK(!newBatch.empty());
            CHECK(newBatch.begin() != batch.end());
            CHECK(newBatch.cbegin() != batch.cend());
            CHECK(newBatch.data() != nullptr);
            CHECK(newBatch.size() == 4);
          }
          THEN("the old batch is empty") {
            CHECK(batch.empty());
            CHECK(batch.begin() == batch.end());
            CHECK(batch.cbegin() == batch.cend());
            CHECK(batch.data() == nullptr);
          }
        }

        WHEN("we move-assign the batch") {
          std::remove_reference_t<decltype(batch)> newBatch(
            texture_type(ogl::g_uninitialised),
            buffer_type(ogl::g_uninitialised),
            GL_TEXTURE0
          );
          newBatch = std::move(batch);
          THEN("the new batch holds the data") {
            CHECK(!newBatch.empty());
            CHECK(newBatch.begin() != batch.end());
            CHECK(newBatch.cbegin() != batch.cend());
            CHECK(newBatch.data() != nullptr);
            CHECK(newBatch.size() == 4);
          }
          THEN("the old batch is empty") {
            CHECK(batch.empty());
            CHECK(batch.begin() == batch.end());
            CHECK(batch.cbegin() == batch.cend());
            CHECK(batch.data() == nullptr);
          }
        }
      }
    }
  }};

  std::size_t spriteCount = 8;
  if (char const* value = std::getenv("SPRITE_COUNT")) {
    std::size_t input = std::strtoul(value, nullptr, 0);
    spriteCount = input == 0 ? 8 : input;
  }

  filtered_batch_type filtered(
    texture_type(0), buffer_type(0), TEXTURE_UNIT);
  testBatch("a filtered batch", filtered, spriteCount);

  unfiltered_batch_type unfiltered(
    texture_type(0), buffer_type(0), TEXTURE_UNIT);
  testBatch("an unfiltered batch", unfiltered, spriteCount);
}
