#include <cstdlib>

#define CATCH_CONFIG_RUNNER
#include "./catch2/catch.hpp"

#include "./init_gl.h"


int main(int argc, char const* const* pArgv) {
  std::vector<char const*> argv(pArgv, pArgv + argc);
  auto const it = std::find_if(argv.begin(), argv.end(), [](char const* arg) {
    return std::strcmp(arg, "--headless") == 0;
  });
  if (it == argv.end()) {
    testing::initGL(); // Will call deinitGL() via atexit.
  }
  else {
    argv.erase(it);
  }
  return Catch::Session().run(argv.size(), argv.data());
}
