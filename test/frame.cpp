#include <catch2/catch.hpp>

#include "spectre/Frame.h"


SCENARIO(
  "Creating a single Frame from a spritesheet",
  "[Frame][spectre::Frame::subframe][no-gui]"
) {
  GIVEN("dimensions for a spritesheet") {
    int const imageWidth = 512;
    int const imageHeight = 1024;
    int const frameWidth = 16;
    int const frameHeight = 64;;
    WHEN("we create a sub-frame with zero offset") {
      auto const frame = spectre::Frame::subframe(
        imageWidth,
        imageHeight,
        frameWidth,
        frameHeight,
        0,
        0
      );

      THEN("the frame coordinates are correct") {
        CHECK(frame.m_uCentre == Approx(0.015625f));
        CHECK(frame.m_vCentre == Approx(0.03125f));
        CHECK(frame.m_frameWidth == Approx(0.03125f));
        CHECK(frame.m_frameHeight == Approx(0.0625f));
      }
    }

    WHEN("we create a sub-frame with non-zero offset") {
      auto const frame = spectre::Frame::subframe(
        imageWidth,
        imageHeight,
        frameWidth,
        frameHeight,
        17,
        19
      );

      THEN("the frame coordinates are correct") {
        CHECK(frame.m_uCentre == Approx(0.015625f + 17.f / 512.f));
        CHECK(frame.m_vCentre == Approx(0.03125f + 19.f / 1024.f));
        CHECK(frame.m_frameWidth == Approx(0.03125f));
        CHECK(frame.m_frameHeight == Approx(0.0625f));
      }
    }
  }
}
