#include <catch2/catch.hpp>

#include "spectre/util.h"


SCENARIO(
  "Creating spritesheet grid frames",
  "[util][spectre::createGridFrames][no-gui]"
) {
  GIVEN("dimensions for a small, square spritesheet") {
    constexpr int count = 4;
    constexpr int imgWidth = 64;
    constexpr int imgHeight = 64;
    constexpr int frameWidth = 32;
    constexpr int frameHeight = 32;

    WHEN("we create grid frames for that spritesheet") {
      auto const frames = spectre::createGridFrames(
        count, imgWidth, imgHeight, frameWidth, frameHeight);

      THEN("the U-coordinates are correct") {
        CHECK(frames[0].m_uCentre == Approx(0.25f));
        CHECK(frames[1].m_uCentre == Approx(0.75f));
        CHECK(frames[2].m_uCentre == Approx(0.25f));
        CHECK(frames[3].m_uCentre == Approx(0.75f));
      }

      THEN("the V-coordinates are correct") {
        CHECK(frames[0].m_vCentre == Approx(0.25f));
        CHECK(frames[1].m_vCentre == Approx(0.25f));
        CHECK(frames[2].m_vCentre == Approx(0.75f));
        CHECK(frames[3].m_vCentre == Approx(0.75f));
      }

      THEN("the frame widths are correct") {
        CHECK(frames[0].m_frameWidth == Approx(0.5f));
        CHECK(frames[1].m_frameWidth == Approx(0.5f));
        CHECK(frames[2].m_frameWidth == Approx(0.5f));
        CHECK(frames[3].m_frameWidth == Approx(0.5f));
      }

      THEN("the frame heights are correct") {
        CHECK(frames[0].m_frameHeight == Approx(0.5f));
        CHECK(frames[1].m_frameHeight == Approx(0.5f));
        CHECK(frames[2].m_frameHeight == Approx(0.5f));
        CHECK(frames[3].m_frameHeight == Approx(0.5f));
      }
    }
  }

  GIVEN("dimensions for a spritesheet with more columns than rows") {
    constexpr int count = 16;
    constexpr int imgWidth = 128;
    constexpr int imgHeight = 64;
    constexpr int frameWidth = 16;
    constexpr int frameHeight = 32;

    WHEN("we create grid frames for that spritesheet") {
      auto const frames = spectre::createGridFrames(
        count, imgWidth, imgHeight, frameWidth, frameHeight);

      THEN("the U-coordinates are correct") {
        CHECK(frames[ 0].m_uCentre == Approx(0.0625f + 0.000f));
        CHECK(frames[ 1].m_uCentre == Approx(0.0625f + 0.125f));
        CHECK(frames[ 2].m_uCentre == Approx(0.0625f + 0.250f));
        CHECK(frames[ 3].m_uCentre == Approx(0.0625f + 0.375f));
        CHECK(frames[ 4].m_uCentre == Approx(0.0625f + 0.500f));
        CHECK(frames[ 5].m_uCentre == Approx(0.0625f + 0.625f));
        CHECK(frames[ 6].m_uCentre == Approx(0.0625f + 0.750f));
        CHECK(frames[ 7].m_uCentre == Approx(0.0625f + 0.875f));

        CHECK(frames[ 8].m_uCentre == Approx(0.0625f + 0.000f));
        CHECK(frames[ 9].m_uCentre == Approx(0.0625f + 0.125f));
        CHECK(frames[10].m_uCentre == Approx(0.0625f + 0.250f));
        CHECK(frames[11].m_uCentre == Approx(0.0625f + 0.375f));
        CHECK(frames[12].m_uCentre == Approx(0.0625f + 0.500f));
        CHECK(frames[13].m_uCentre == Approx(0.0625f + 0.625f));
        CHECK(frames[14].m_uCentre == Approx(0.0625f + 0.750f));
        CHECK(frames[15].m_uCentre == Approx(0.0625f + 0.875f));
      }

      THEN("the V-coordinates are correct") {
        CHECK(frames[ 0].m_vCentre == Approx(0.25f));
        CHECK(frames[ 1].m_vCentre == Approx(0.25f));
        CHECK(frames[ 2].m_vCentre == Approx(0.25f));
        CHECK(frames[ 3].m_vCentre == Approx(0.25f));
        CHECK(frames[ 4].m_vCentre == Approx(0.25f));
        CHECK(frames[ 5].m_vCentre == Approx(0.25f));
        CHECK(frames[ 6].m_vCentre == Approx(0.25f));
        CHECK(frames[ 7].m_vCentre == Approx(0.25f));

        CHECK(frames[ 8].m_vCentre == Approx(0.75f));
        CHECK(frames[ 9].m_vCentre == Approx(0.75f));
        CHECK(frames[10].m_vCentre == Approx(0.75f));
        CHECK(frames[11].m_vCentre == Approx(0.75f));
        CHECK(frames[12].m_vCentre == Approx(0.75f));
        CHECK(frames[13].m_vCentre == Approx(0.75f));
        CHECK(frames[14].m_vCentre == Approx(0.75f));
        CHECK(frames[15].m_vCentre == Approx(0.75f));
      }

      THEN("the frame widths are correct") {
        for (int i = 0; i < count; ++i) {
          REQUIRE(frames[i].m_frameWidth == Approx(0.125f));
        }
      }

      THEN("the frame heights are correct") {
        for (int i = 0; i < count; ++i) {
          REQUIRE(frames[i].m_frameHeight == Approx(0.5f));
        }
      }
    }
  }

  GIVEN("dimensions for a spritesheet with more rows than columns") {
    constexpr int count = 8;
    constexpr int imgWidth = 128;
    constexpr int imgHeight = 512;
    constexpr int frameWidth = 64;
    constexpr int frameHeight = 128;

    WHEN("we create grid frames for that spritesheet") {
      auto const frames = spectre::createGridFrames(
        count, imgWidth, imgHeight, frameWidth, frameHeight);

      THEN("the U-coordinates are correct") {
        CHECK(frames[ 0].m_uCentre == Approx(0.25f));
        CHECK(frames[ 1].m_uCentre == Approx(0.75f));

        CHECK(frames[ 2].m_uCentre == Approx(0.25f));
        CHECK(frames[ 3].m_uCentre == Approx(0.75f));

        CHECK(frames[ 4].m_uCentre == Approx(0.25f));
        CHECK(frames[ 5].m_uCentre == Approx(0.75f));

        CHECK(frames[ 6].m_uCentre == Approx(0.25f));
        CHECK(frames[ 7].m_uCentre == Approx(0.75f));
      }

      THEN("the V-coordinates are correct") {
        CHECK(frames[ 0].m_vCentre == Approx(0.125f + 0.00f));
        CHECK(frames[ 1].m_vCentre == Approx(0.125f + 0.00f));
        CHECK(frames[ 2].m_vCentre == Approx(0.125f + 0.25f));
        CHECK(frames[ 3].m_vCentre == Approx(0.125f + 0.25f));
        CHECK(frames[ 4].m_vCentre == Approx(0.125f + 0.50f));
        CHECK(frames[ 5].m_vCentre == Approx(0.125f + 0.50f));
        CHECK(frames[ 6].m_vCentre == Approx(0.125f + 0.75f));
        CHECK(frames[ 7].m_vCentre == Approx(0.125f + 0.75f));
      }

      THEN("the frame widths are correct") {
        for (int i = 0; i < count; ++i) {
          REQUIRE(frames[i].m_frameWidth == Approx(0.5));
        }
      }

      THEN("the frame heights are correct") {
        for (int i = 0; i < count; ++i) {
          REQUIRE(frames[i].m_frameHeight == Approx(0.25f));
        }
      }
    }
  }
}


SCENARIO(
  "Creating grid frames for a section of a spritesheet",
  "[util][spectre::createGridFramesSubSection][no-gui]"
) {
  GIVEN("dimensions for a small spritesheet") {
    constexpr int count = 4;
    constexpr int numColumns = 2;
    constexpr int imgWidth = 512;
    constexpr int imgHeight = 1024;
    constexpr int frameWidth = 32;
    constexpr int frameHeight = 16;
    constexpr int uOffset = 128;
    constexpr int vOffset = 256;

    WHEN("we create frames for a section of that spritesheet") {
      auto const frames = spectre::createGridFramesSubSection(
        count, numColumns, imgWidth, imgHeight, frameWidth, frameHeight,
        uOffset, vOffset);

      THEN("the U-coordinates are correct") {
        CHECK(frames[0].m_uCentre == Approx(0.28125f));
        CHECK(frames[1].m_uCentre == Approx(0.34375f));
        CHECK(frames[2].m_uCentre == Approx(0.28125f));
        CHECK(frames[3].m_uCentre == Approx(0.34375f));
      }

      THEN("the V-coordinates are correct") {
        CHECK(frames[0].m_vCentre == Approx(0.2578125f));
        CHECK(frames[1].m_vCentre == Approx(0.2578125f));
        CHECK(frames[2].m_vCentre == Approx(0.2734375f));
        CHECK(frames[3].m_vCentre == Approx(0.2734375f));
      }

      THEN("the frame widths are correct") {
        CHECK(frames[0].m_frameWidth == Approx(0.0625));
        CHECK(frames[1].m_frameWidth == Approx(0.0625));
        CHECK(frames[2].m_frameWidth == Approx(0.0625));
        CHECK(frames[3].m_frameWidth == Approx(0.0625));
      }

      THEN("the frame heights are correct") {
        CHECK(frames[0].m_frameHeight == Approx(0.015625f));
        CHECK(frames[1].m_frameHeight == Approx(0.015625f));
        CHECK(frames[2].m_frameHeight == Approx(0.015625f));
        CHECK(frames[3].m_frameHeight == Approx(0.015625f));
      }
    }
  }
}
