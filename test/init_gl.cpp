#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>

#include "./init_gl.h"


namespace testing {


enum ErrorCode {
  ERR_SDL_INIT_FAILED = 1,
  ERR_WINDOW_INIT_FAILED = 2,
  ERR_GL_CONTEXT_INIT_FAILED = 3,
  ERR_GL_ERROR_ON_CONTEXT_CREATION = 4,
  ERR_GLEW_INIT_FAILED = 5,
};


bool
checkGLErrors(char const* const pFile, unsigned long line) {
  bool foundErrors = false;
  GLenum error;
  while ((error = glGetError())) {
    std::cerr << "GL error [" << pFile << ':' << line << "] "
      << glewGetErrorString(error) << std::endl;
    foundErrors = true;
  }
  return foundErrors;
}


GLuint g_vao = 0;
SDL_Window* g_window;
SDL_GLContext g_glContext;


void deinitGL() {
  glDeleteVertexArrays(1, &g_vao);
  if (g_glContext) {
    SDL_GL_DeleteContext(g_glContext);
    g_glContext = nullptr;
  }
  if (g_window) {
    SDL_DestroyWindow(g_window);
    g_window = nullptr;
  }
  SDL_Quit();
}


int initGL() {
  atexit(deinitGL);
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
    std::cerr << "SDL init failed" << std::endl;
    return ERR_SDL_INIT_FAILED;
  }

  // Window.
  constexpr Uint32 windowWidth = 640;
  constexpr Uint32 windowHeight = 480;
  g_window = SDL_CreateWindow(
    "Spectre demo",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    windowWidth,
    windowHeight,
    SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN
  );
  if (!g_window) {
    std::cerr << "Failed to create SDL window" << std::endl;
    return ERR_WINDOW_INIT_FAILED;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(
      SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  // OpenGL context.
  g_glContext = SDL_GL_CreateContext(g_window);
  if (!g_glContext) {
    std::cerr << "Failed to create GL context" << std::endl;
    return ERR_GL_CONTEXT_INIT_FAILED;
  }
  if (auto const error = glGetError(); error != GL_NO_ERROR) {
    std::cerr << "GL error: " << error << std::endl;
    return ERR_GL_ERROR_ON_CONTEXT_CREATION;
  }

  // GLEW initialisation.
  glewExperimental = GL_TRUE;
  if (GLenum const glewError = glewInit(); glewError != GLEW_OK) {
    std::cerr << "GLEW error: " << glewGetErrorString(glewError);
    return ERR_GLEW_INIT_FAILED;
  }

  glGenVertexArrays(1, &g_vao);

  return 0;
}


} // namespace testing
