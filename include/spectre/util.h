#ifndef SPECTRE_INCLUDE__SPECTRE__UTIL__H
#define SPECTRE_INCLUDE__SPECTRE__UTIL__H
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include "./Frame.h"


namespace spectre {


/// Assign equal-sized square frames for a spritesheet to a buffer.
void
assignGridFrames(
  Frame* frames,
  int count,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight
);


/// Create a vector of frames for a spritesheet of equal-sized square frames.
inline std::unique_ptr<Frame[]>
createGridFrames(
  int count,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight
) {
  auto frames = std::make_unique<Frame[]>(count);
  assignGridFrames(
    frames.get(), count, imgWidth, imgHeight, frameWidth, frameHeight);
  return frames;
}


/// Assign equal-sized square frames for a spritesheet sub-section to a buffer.
void
assignGridFramesSubSection(
  Frame* frames,
  int count,
  int numColumns,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight,
  int uOffset,
  int vOffset
);


/// Create a vector of frames for a subsection of a spritesheet.
inline std::unique_ptr<Frame[]>
createGridFramesSubSection(
  int count,
  int numColumns,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight,
  int uOffset,
  int vOffset
) {
  auto frames = std::make_unique<Frame[]>(count);
  assignGridFramesSubSection(
    frames.get(), count, numColumns, imgWidth, imgHeight,
    frameWidth, frameHeight, uOffset, vOffset);
  return frames;
}


/// Initialise a texture and buffer, and bind together for a texture buffer.
void
initTextureBuffer(
  GLuint textureId,
  GLenum textureUnit,
  GLuint bufferId,
  void const* data,
  std::size_t numBytes
);


} // namespace spectre
#endif // #ifndef SPECTRE_INCLUDE__SPECTRE__UTIL__H
