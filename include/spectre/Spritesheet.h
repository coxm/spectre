#ifndef SPECTRE_INCLUDE__SPECTRE__SPRITESHEET__H
#define SPECTRE_INCLUDE__SPECTRE__SPRITESHEET__H
#include <utility>
#include <initializer_list>
#include <algorithm> // std::move

#include <ogl/ownership/Unique.h>
#include <ogl/Texture.h>
#include <ogl/Buffer.h>


namespace spectre {


void
initTextureBuffer(
  GLuint textureId,
  GLenum textureUnit,
  GLuint bufferId,
  void const* data,
  std::size_t numBytes
);


template <
  typename BufferT = ogl::Buffer<ogl::ownership::Unique>,
  typename BufferTextureT = ogl::Texture<ogl::ownership::Unique>,
  typename ImageTextureT = ogl::Texture<ogl::ownership::Unique>
>
class Spritesheet {
public:
  using buffer_type = BufferT;
  using buffer_texture_type = BufferTextureT;
  using image_texture_type = ImageTextureT;

  inline Spritesheet(
    ogl::uninitialised_tag v,
    GLenum imageTexUnit,
    GLenum bufferTexUnit
  )
    : m_imgTex(v)
    , m_bufferTex(v)
    , m_buffer(v)
    , m_imageTexUnit(imageTexUnit)
    , m_bufferTexUnit(bufferTexUnit)
  {
  }

  inline Spritesheet(
    ogl::initialised_tag v,
    GLenum imageTexUnit,
    GLenum bufferTexUnit
  )
    : m_imgTex(v)
    , m_bufferTex(v)
    , m_buffer(v)
    , m_imageTexUnit(imageTexUnit)
    , m_bufferTexUnit(bufferTexUnit)
  {
  }

  inline Spritesheet(
    image_texture_type&& imageTexture,
    GLenum imageTexUnit,
    buffer_texture_type&& bufferTexture,
    buffer_type&& buffer,
    GLenum bufferTexUnit
  )
    : m_imgTex{std::move(imageTexture)}
    , m_bufferTex{std::move(bufferTexture)}
    , m_buffer{std::move(buffer)}
    , m_imageTexUnit(imageTexUnit)
    , m_bufferTexUnit(bufferTexUnit)
  {
  }

  inline image_texture_type& imageTexture() noexcept { return m_imgTex; }
  inline image_texture_type const& imageTexture() const noexcept
  { return m_imgTex; }

  inline buffer_texture_type& bufferTexture() noexcept { return m_bufferTex; }
  inline buffer_texture_type const& bufferTexture() const noexcept
  { return m_bufferTex; }

  inline buffer_type& buffer() noexcept { return m_buffer; }
  inline buffer_type const& buffer() const noexcept { return m_buffer; }

  inline GLenum imageTextureUnit() const noexcept { return m_imageTexUnit; }
  inline GLenum setImageTextureUnit() const noexcept { return m_imageTexUnit; }

  inline GLenum bufferTextureUnit() const noexcept { return m_bufferTexUnit; }
  inline GLenum setBufferTextureUnit() const noexcept
  { return m_bufferTexUnit; }

  template <typename FrameT>
  inline void
  assignFrames(std::initializer_list<FrameT> frames) noexcept {
    assignFrames(frames.begin(), frames.size());
  }

  template <typename FrameT>
  inline void
  assignFrames(FrameT const* data, std::size_t count) noexcept {
    assign(static_cast<void const*>(data), count * sizeof(FrameT));
  }

  inline void
  assign(void const* data, std::size_t bytes) noexcept {
    initTextureBuffer(
      m_bufferTex.id(),
      m_bufferTexUnit,
      m_buffer.id(),
      static_cast<void const*>(data),
      bytes);
  }

private:
  image_texture_type m_imgTex;
  buffer_texture_type m_bufferTex;
  buffer_type m_buffer;
  GLenum m_imageTexUnit;
  GLenum m_bufferTexUnit;
};


} // namespace spectre
#endif // #ifndef SPECTRE_INCLUDE__SPECTRE__SPRITESHEET__H
