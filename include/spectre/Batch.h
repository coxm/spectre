#ifndef SPECTRE_INCLUDE__SPECTRE__BATCH__H
#define SPECTRE_INCLUDE__SPECTRE__BATCH__H
#include <cassert>
#include <memory>
#include <tuple>
#include <type_traits>
#include <algorithm> // std::move

#include <ogl/ownership/Unique.h>
#include <ogl/Texture.h>
#include <ogl/Buffer.h>


namespace spectre {


struct Unfiltered {};
using UnfilteredTag = Unfiltered;


void initTextureBuffer(
  GLuint textureId,
  GLenum textureUnit,
  GLuint bufferId,
  void const* data,
  std::size_t bytes
);


/**
 * Sprite batch class.
 *
 * @tparam Sprite a type which holds relevant (and frequently updating) data
 * about a sprite, e.g. position, scale, rotation and possibly animation frame
 * indices. This data is constantly streamed to the GPU, so static data (such
 * as animation frame coordinates) is better stored in the frame type of a
 * @ref spectre::Spritesheet.
 *
 * @tparam Filter a callable type which returns @c true when called with a
 * sprite which should be rendered; or @c UnfilteredTag, if no filtering is
 * desired.
 *
 * @tparam Buffer the type used to manage a buffer object's lifetime.
 *
 * @tparam Texture the type used to manage a texture object's lifetime.
 */
template <
  typename Sprite,
  typename Filter,
  typename Buffer = ogl::Buffer<ogl::ownership::Unique>,
  typename Texture = ogl::Texture<ogl::ownership::Unique>,
  typename SpriteAllocatorT = std::allocator<Sprite>
>
class Batch {
public:
  using value_type = Sprite;
  using reference = value_type&;
  using const_reference = value_type&;
  using pointer = value_type*;
  using const_pointer = value_type const*;
  using iterator = pointer;
  using const_iterator = const_pointer;
  using allocator_type = SpriteAllocatorT;

  using filter_type = Filter;
  using buffer_type = Buffer;
  using texture_type = Texture;

  static constexpr bool s_filteringEnabled =
    !std::is_same_v<filter_type, UnfilteredTag>;

  inline Batch(
    ogl::initialised_tag tag,
    GLenum textureUnit,
    filter_type filter = filter_type(),
    allocator_type alloc = allocator_type()
  )
    : m_size(0)
    , m_pSprites{nullptr}
    , m_texture{tag}
    , m_buffer{tag}
    , m_tuple{makeTuple(std::move(alloc), std::move(filter), textureUnit)}
  {
  }

  inline Batch(
    texture_type&& texture,
    buffer_type&& buffer,
    GLenum textureUnit, 
    filter_type filter = filter_type(),
    allocator_type alloc = allocator_type()
  )
    : m_size(0)
    , m_pSprites{nullptr}
    , m_texture{std::move(texture)}
    , m_buffer{std::move(buffer)}
    , m_tuple{makeTuple(std::move(alloc), std::move(filter), textureUnit)}
  {
  }

  Batch(Batch const&) = delete;
  Batch(Batch&& other) noexcept
    : m_size(std::exchange(other.m_size, 0))
    , m_pSprites(std::exchange(other.m_pSprites, nullptr))
    , m_texture(std::move(other.m_texture))
    , m_buffer(std::move(other.m_buffer))
    , m_tuple(std::move(other.m_tuple))
  {
  }

  Batch& operator=(Batch const&) = delete;
  Batch& operator=(Batch&& other) noexcept {
    m_size = std::exchange(other.m_size, 0);
    m_pSprites = std::exchange(other.m_pSprites, nullptr);
    m_texture = std::move(other.m_texture);
    m_buffer = std::move(other.m_buffer);
    m_tuple = std::move(other.m_tuple);
    return *this;
  }

  ~Batch() noexcept {
    clear();
  }

  inline buffer_type& buffer() noexcept { return m_buffer; }
  inline buffer_type const& buffer() const noexcept { return m_buffer; }

  inline texture_type& texture() noexcept { return m_texture; }
  inline texture_type const& texture() const noexcept { return m_texture; }

  inline GLenum textureUnit() const noexcept
  { return std::get<s_texUnitIdx>(m_tuple); }

  inline void setTextureUnit(GLenum unit) noexcept
  { std::get<s_texUnitIdx>(m_tuple) = unit; }

  inline filter_type& filter() noexcept
  { return std::get<s_filterIdx>(m_tuple); }
  inline filter_type const& filter() const noexcept
  { return std::get<s_filterIdx>(m_tuple); }

  inline allocator_type& allocator() noexcept
  { return std::get<s_allocIdx>(m_tuple); }
  inline allocator_type const& allocator() const noexcept
  { return std::get<s_allocIdx>(m_tuple); }

  inline std::size_t size() const noexcept {
    return m_size;
  }

  inline std::size_t numVisible() const noexcept {
    if constexpr(s_filteringEnabled) return std::get<s_numVisibleIdx>(m_tuple);
    else return m_size;
  }

  inline void
  assign(std::size_t count, value_type const& transform) {
    doResizeSprites(count);
    std::fill_n(m_pSprites, count, transform);
    initBuffer();
  }

  inline void
  assign(std::initializer_list<value_type> ilist) {
    doResizeSprites(ilist.size());
    std::copy(std::begin(ilist), std::end(ilist), m_pSprites);
    initBuffer();
  }

  template <typename ForwardIter>
  inline void
  assign(ForwardIter iBegin, ForwardIter iEnd) {
    doResizeSprites(std::distance(iBegin, iEnd));
    std::copy(iBegin, iEnd, m_pSprites);
    initBuffer();
  }

  inline reference
  at(std::size_t i) {
    assert((i < m_size) && "Invalid sprite index");
    return m_pSprites[i];
  }

  inline const_reference
  at(std::size_t i) const {
    assert((i < m_size) && "Invalid sprite index");
    return m_pSprites[i];
  }

  /**
   * @defgroup raw_access_methods
   * @{
   */
  inline pointer data() noexcept { return m_pSprites; }
  inline const_pointer data() const noexcept { return m_pSprites; }

  inline iterator begin() noexcept { return m_pSprites; }
  inline const_iterator begin() const noexcept { return m_pSprites; }
  inline const_iterator cbegin() const noexcept { return m_pSprites; }

  inline iterator end() noexcept
  { return m_pSprites + m_size; }
  inline const_iterator end() const noexcept
  { return m_pSprites + m_size; }
  inline const_iterator cend() const noexcept
  { return m_pSprites + m_size; }
  /// @}

  inline void initBuffer() const noexcept {
    initTextureBuffer(
      m_texture.id(),
      textureUnit(),
      m_buffer.id(),
      static_cast<void const*>(m_pSprites),
      m_size * sizeof(value_type)
    );
  }

  /**
   * Update the sprite batch, pushing data to the GL buffer.
   *
   * If this batch is filtered, only data passing the filter test will be sent.
   */
  inline void update() {
    glBindBuffer(GL_TEXTURE_BUFFER, m_buffer.id());
    if constexpr(s_filteringEnabled) {
      updateFilteredEndSection(0);
    }
    else {
      updateUnfilteredSection(0, m_size);
    }
  }

  /**
   * Update a section of the sprite collection without filtering sprites.
   *
   * @param first the index of the first sprite to update.
   * @param count the number of sprites to update.
   */
  inline void updateUnfilteredSection(GLint first, GLsizei count) {
    // No glBindBuffer call: since multiple sections are likely to be updated
    // at the same, we'll let client code call glBindBuffer exactly once.
    glBufferSubData(
      GL_TEXTURE_BUFFER,
      GLintptr(first * sizeof(value_type)),
      GLsizeiptr(count * sizeof(value_type)),
      m_pSprites + first);
  }

  /**
   * Update an end section of the collection, with filtering if enabled.
   *
   * If filtering is not enabled, this is the same as calling
   * `updateUnfilteredSection(first, size() - first)`.
   *
   * This method is safe to use with the `render` method.
   */
  inline void updateFilteredEndSection(GLint first) {
    updateFilteredSection(first, m_size - first);
  }

  /**
   * Update an arbitrary section of the collection with filtering, if enabled.
   *
   * If filtering is not enabled, this is the same as calling
   * `updateUnfilteredSection(first, count)`.
   *
   * If filtering is enabled, calling this method wil mean multiple draw calls
   * are required (e.g. using `renderSection`). Using `render()` will result in
   * any filtered-out sprites still being sent to the buffer, since `render()`
   * sends a contigious region of memory via `glDrawArrays`.
   */
  GLsizei updateFilteredSection(GLint first, GLsizei count) {
    if constexpr(s_filteringEnabled) {
      // No glBindBuffer call: since multiple sections are likely to be updated
      // at the same, we'll let client code call glBindBuffer exactly once.
      assert(((std::size_t(first + count) <= m_size) && "Bad section size"));
      auto* const pSplit = m_pSprites + m_size;
      auto* const pEnd = std::copy_if(
        m_pSprites + first, pSplit, pSplit, filter());
      auto const visibleCount = first + (pEnd - pSplit);
      std::get<s_numVisibleIdx>(m_tuple) = visibleCount;
      glBufferSubData(
        GL_TEXTURE_BUFFER,
        first * sizeof(value_type),
        visibleCount * sizeof(value_type),
        pSplit);
      return visibleCount;
    }
    else {
      updateUnfilteredSection(first, m_size);
      return m_size - first;
    }
  }

  /**
   * Render all sprites which have not been filtered out.
   *
   * If using `updateFilteredSection`, prefer separate `renderSection` calls.
   * @see updateFilteredSection.
   */
  inline void render() const noexcept {
    glDrawArrays(GL_TRIANGLES, 0, 6 * numVisible());
  }

  inline void renderSection(GLint first, GLsizei count) const noexcept {
    assert(((0 <= first && (first + count) <= numVisible()) &&
           "Invalid renderSub index or count"));
    glDrawArrays(GL_TRIANGLES, first * 6, count * 6);
  }

  inline bool empty() const noexcept {
    return m_pSprites == nullptr;
  }

  inline void clear() noexcept {
    if (m_pSprites != nullptr) {
      allocator().deallocate(m_pSprites, bufferSize(m_size));
    }
    m_size = 0;
    m_pSprites = nullptr;
  }

private:
  using tuple_type = std::conditional_t<
    s_filteringEnabled,
    std::tuple<allocator_type, Filter, GLenum, unsigned>,
    std::tuple<allocator_type, Filter, GLenum>
  >;

  constexpr static std::size_t s_allocIdx = 0;
  constexpr static std::size_t s_filterIdx = 1;
  constexpr static std::size_t s_texUnitIdx = 2;
  constexpr static std::size_t s_numVisibleIdx = 3;

  static inline tuple_type
  makeTuple(allocator_type&& alloc, filter_type&& filter, GLenum texUnit) {
    if constexpr(s_filteringEnabled) {
      return tuple_type(std::move(alloc), std::move(filter), texUnit, 0);
    }
    else {
      return tuple_type(std::move(alloc), std::move(filter), texUnit);
    }
  }

  static inline std::size_t bufferSize(std::size_t count) noexcept {
    if constexpr(s_filteringEnabled) return count * 2;
    else return count;
  }

  inline void doResizeSprites(std::size_t count) {
    if (m_pSprites != nullptr) {
      allocator().deallocate(m_pSprites, bufferSize(m_size));
    }
    m_size = count;
    m_pSprites = allocator().allocate(bufferSize(count));
  }

  std::size_t m_size;
  Sprite* m_pSprites;
  texture_type m_texture;
  buffer_type m_buffer;
  tuple_type m_tuple;
};


} // namespace spectre
#endif // #ifndef SPECTRE_INCLUDE__SPECTRE__BATCH__H
