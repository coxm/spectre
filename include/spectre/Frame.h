#ifndef SPECTRE_INCLUDE__SPECTRE__FRAME__H
#define SPECTRE_INCLUDE__SPECTRE__FRAME__H
namespace spectre {


struct Frame {
  float m_uCentre;
  float m_vCentre;
  float m_frameWidth;
  float m_frameHeight;

  /// Create a Frame as a sub-frame of a larger spritesheet.
  inline static Frame subframe(
    int imageWidth,
    int imageHeight,
    int frameWidth,
    int frameHeight,
    int uOffset,
    int vOffset
  ) {
    return {
      float(uOffset + frameWidth  / 2) / imageWidth,
      float(vOffset + frameHeight / 2) / imageHeight,
      float(frameWidth ) / imageWidth,
      float(frameHeight) / imageHeight,
    };
  }
};


} // namespace spectre
#endif // #ifndef SPECTRE_INCLUDE__SPECTRE__FRAME__H
