# Spectre

A C++ library for batching and rendering sprites with OpenGL. Avoids sending
redundant vertex data to the GPU, using a texture buffer.

## Usage

### Basic usage
```cpp
#include <spectre/Spritesheet.h>
#include <spectre/Batch.h>
#include <spectre/util.h>

class Sprite { ... };

// Create a spritesheet texture.
spectre::Spritesheet ss(
    ogl::g_initialised, imageTextureUnit, bufferTextureUnit);

// Define simple grid-based frames for the spritesheet image.
auto const frames = spectre::createGridFrames(
    frameCount, imageWidth, imageHeight, frameWidth, frameHeight);
spritesheet.assignFrames(frames.get(), frameCount);

// Create a sprite batch.
using Batch = spectre::Batch<Sprite, spectre::UnfilteredTag>;
Batch batch(ogl::g_initialised, GL_TEXTURE2);
batch.assign(spritesBegin, spritesEnd); // Other overloads available.

// Update sprite data.
Sprite* const sprites = batch.data();
// Call batch.update() so the Batch knows the sprites have changed.
batch.update();

// Render sprite data.
batch.render();
```

### Filtering

Filtering can be added to a `spectre::Batch` by passing a filter type as the
second template parameter:

```cpp
struct Filter {
    inline bool operator()(Sprite const& sprite) const noexcept {
        // Return true if the sprite should be rendered.
    }
};

using Batch = spectre::Batch<Sprite, Filter>;
```

For more specific details, see [demo/sprites.cpp](demo/sprites.cpp).

## Building

The project can be built with CMake, for example using the following steps.

    mkdir -p spectre/build && cd spectre
    git clone https://gitlab.com/coxm/spectre .
    cd build && cmake -DCMAKE_BUILD_TYPE=Release ..
    make

## Installation in a CMake project

Spectre can be downloaded using CMake's FetchContent, or using another method
such as Git submodules. The CMake targets can be imported via
`add_subdirectory`. Although Spectre has an external dependency
([ogl](../../coxm/ogl)), that library is automatically downloaded using
FetchContent if not detected.

The library can then be added as a target in the usual way:

    target_link_libraries(my-executable PUBLIC spectre::spectre)

## Installation in a non-CMake project

If not using CMake, you should compile (see [Building](#building) above) and
link as appropriate.

## Installation to the system

Build the project as above (see [#building](Building) above). In the build
directory, run the additional installation step, for example:

    make install

## Demo

An example project is included in the `demo` directory. This shows how to
include and build Spectre as part of another CMake project, and then renders a
number of sprites, with a custom data type and different transforms.
