find_package(SDL2 REQUIRED)
find_package(glm REQUIRED)


add_library(stbimage "${CMAKE_CURRENT_SOURCE_DIR}/stb_image.cpp")


add_library(spectre-demolib INTERFACE)
target_include_directories(spectre-demolib INTERFACE
  ${SDL2_INCLUDE_DIRS} ${GLM_INCLUDE_DIRS})
target_link_libraries(spectre-demolib INTERFACE
  spectre::spectre ogl::ogl stbimage ${SDL2_LIBRARIES})
target_compile_features(spectre-demolib INTERFACE cxx_std_17)
target_compile_definitions(spectre-demolib INTERFACE
  VERT_SHADER_PATH="${CMAKE_CURRENT_SOURCE_DIR}/gls/basic.vert"
  FRAG_SHADER_PATH="${CMAKE_CURRENT_SOURCE_DIR}/gls/basic.frag"
  SPRITESHEET_PATH="${CMAKE_CURRENT_BINARY_DIR}/spritesheet.png")
target_compile_options(spectre-demolib INTERFACE ${spectre_COMPILE_OPTIONS})
target_compile_features(spectre-demolib INTERFACE cxx_std_17)


add_executable(spectre-demo-sprites "${CMAKE_CURRENT_SOURCE_DIR}/sprites.cpp")
set_target_properties(spectre-demo-sprites PROPERTIES OUTPUT_NAME sprites)
target_link_libraries(spectre-demo-sprites spectre-demolib)


find_program(INKSCAPE_BIN inkscape)
find_program(CONVERT_BIN convert)
if(INKSCAPE_BIN)
  set(DEMO_SVG_COMMAND
    ${INKSCAPE_BIN}
      --export-png="${CMAKE_CURRENT_BINARY_DIR}/spritesheet.png"
      "${CMAKE_CURRENT_SOURCE_DIR}/spritesheet.svg")
elseif(CONVERT_BIN)
  set(DEMO_SVG_COMMAND
    ${CONVERT_BIN}
      "${CMAKE_CURRENT_SOURCE_DIR}/spritesheet.svg"
      "${CMAKE_CURRENT_BINARY_DIR}/spritesheet.png")
else()
  message(FATAL_ERROR "Failed to find suitable SVG -> PNG converter")
endif()


add_custom_command(
  OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/spritesheet.png"
  COMMAND ${DEMO_SVG_COMMAND}
  MAIN_DEPENDENCY "${CMAKE_CURRENT_SOURCE_DIR}/spritesheet.svg"
  COMMENT "Rendering spritesheet"
  USES_TERMINAL)
add_custom_target(spectre-demo-heet ALL DEPENDS
  "${CMAKE_CURRENT_BINARY_DIR}/spritesheet.png")
