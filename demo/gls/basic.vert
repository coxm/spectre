# version 330 core
// For each quad, the centre UV coordinate, and the texture dimensions.
uniform samplerBuffer frames;
// The sprite data. See demo/main.cpp for the layout.
uniform samplerBuffer actors;

uniform mat4 mvp;


// The texture coordinates for this vertex.
out vec4 uv;
// The tweening factor.
out float tween;


// Multipliers for calculating the position of each corner in a quad.
const vec2 corners[6] = vec2[](
  vec2(-0.5f, -0.5f),
  vec2( 0.5f, -0.5f),
  vec2( 0.5f,  0.5f),
  vec2( 0.5f,  0.5f),
  vec2(-0.5f,  0.5f),
  vec2(-0.5f, -0.5f)
);


vec3 applyQuat(vec4 quat, vec3 vec) {
  return vec + 2.0f * cross(quat.xyz, cross(quat.xyz, vec) + quat.w * vec);
}


const int ACTOR_VEC4_COUNT = 3;


void main() {
  int cornerIdx = gl_VertexID % 6;
  vec2 corner = corners[cornerIdx];

  // Retrieve vertex data from the actors texture buffer.
  int bufferOffset = ACTOR_VEC4_COUNT * (gl_VertexID / 6);
  vec4 xyzt = texelFetch(actors, bufferOffset);
  vec4 quat = texelFetch(actors, bufferOffset + 1);
  vec4 data = texelFetch(actors, bufferOffset + 2);

  // Unpack vertex data.
  vec3 quadCentre = xyzt.xyz;
  tween = xyzt.w;
  vec2 scale = data.xy;
  int thisFrameIdx = floatBitsToInt(data.z);
  int nextFrameIdx = floatBitsToInt(data.w);

  // Calculate the vertex position.
  vec3 vertexOffset = applyQuat(quat, vec3(scale * corner, 0.0f));
  vec4 localPos = vec4(quadCentre + vertexOffset, 1.0f);
  gl_Position = mvp * localPos;

  // Calculate the UV coordinates.
  vec4 thisFrame = texelFetch(frames, thisFrameIdx);
  vec4 nextFrame = texelFetch(frames, nextFrameIdx);
  uv = vec4(
    thisFrame.xy + corner * thisFrame.zw,
    nextFrame.xy + corner * nextFrame.zw);
}
