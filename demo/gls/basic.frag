# version 330 core
uniform sampler2D spritesheet;

in vec4 uv;
in float tween;
out vec4 colour;


void main() {
  vec4 thisColour = texture(spritesheet, uv.xy);
  vec4 nextColour = texture(spritesheet, uv.zw);
  colour = mix(thisColour, nextColour, tween);
}
