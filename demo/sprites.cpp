/**
 * @file demo/main.cpp
 *
 * Renders a batch of sprites using a @ref spectre::Batch.
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

#include <ogl/Texture.h>
#include <ogl/Program.h>
#include <ogl/Vao.h>
#include <ogl/uniforms.h>
#include <ogl/errors/check.h>

#include <spectre/util.h>
#include <spectre/Spritesheet.h>
#include <spectre/Batch.h>

#include "./stb_image.h"


enum ErrorCode {
  ERR_SDL_INIT_FAILED = 1,
  ERR_WINDOW_INIT_FAILED = 2,
  ERR_GL_CONTEXT_INIT_FAILED = 3,
  ERR_GL_ERROR_ON_CONTEXT_CREATION = 4,
  ERR_GL_SHADER_COMPILATION_FAILED = 5,
  ERR_GL_PROGRAM_CREATION_FAILED = 6,
  ERR_GL_PROGRAM_LINK_FAILED = 7,
  ERR_GLEW_INIT_FAILED = 8,
  ERR_GL_CONFIG_FAILED = 9,
  ERR_SPRITESHEET_LOAD_FAILED = 10,
  ERR_TEX_BUFFER_ERROR = 11,
  ERR_UNIFORM_SET_ERROR = 12,
};


int
getIntegerEnvVar(char const* pName, int defaultValue = 0) {
  char const* const pValue = std::getenv(pName);
  if (pValue) {
    return std::atoi(pName);
  }
  // We can't distinguish between an error and actual zero values here but it
  // doesn't matter for our uses.
  return defaultValue;
}


bool
checkGLErrors(char const* const pFile, unsigned long line) {
  bool foundErrors = false;
  GLenum error;
  while ((error = glGetError())) {
    std::cerr << "GL error [" << pFile << ':' << line << "] "
      << glewGetErrorString(error) << std::endl;
    foundErrors = true;
  }
  return foundErrors;
}


std::string
loadTextFile(char const* const pFilePath) {
  std::ifstream ifs{pFilePath};
  if (!ifs.good()) {
    throw std::runtime_error{std::string("Can't load file: ") + pFilePath};
  }
  std::stringstream buffer;
  buffer << ifs.rdbuf();
  return buffer.str();
}


struct SDLWindowDeleter {
  inline void operator()(SDL_Window* window) const noexcept {
    SDL_DestroyWindow(window);
  }
};


struct SDLGLContextDeleter {
  inline void operator()(SDL_GLContext context) const noexcept {
    SDL_GL_DeleteContext(context);
  }
};


struct Sprite {
  struct Filter {
    inline bool operator()(Sprite const& actor) const noexcept {
      return actor.m_thisFrameIdx >= 0;
    }
  };

  inline Sprite(
    glm::vec3 pos = glm::vec3(0.f, 0.f, 0.f),
    int thisFrameIdx = 0,
    glm::quat quat = glm::angleAxis(0.f, glm::vec3(0.f, 0.f, 1.f)),
    glm::vec2 scale = glm::vec2(1.f, 1.f)
  )
    noexcept
    : m_pos(pos)
    , m_tween(0.f)
    , m_quat(quat)
    , m_scale(scale)
    , m_thisFrameIdx(thisFrameIdx)
    , m_nextFrameIdx(thisFrameIdx)
  {
  }

  void setFrame(int index) noexcept {
    m_thisFrameIdx = index;
    m_nextFrameIdx = (index + 1) % 4;
  }

  glm::vec3 m_pos;
  float m_tween;

  glm::quat m_quat;

  glm::vec2 m_scale;
  int m_thisFrameIdx;
  int m_nextFrameIdx;
};


int
loadImage(
  GLenum textureUnit,
  GLuint textureId,
  int* pImgWidth,
  int* pImgHeight
) {
  char const* spritesheetPath = std::getenv("spritesheet");
  if (spritesheetPath == nullptr) {
    spritesheetPath = SPRITESHEET_PATH;
  }
  stbi_set_flip_vertically_on_load(true);
  unsigned char* pData = stbi_load(
    spritesheetPath, pImgWidth, pImgHeight, nullptr,
    4 // 0 to automatically choose number of components; or 1, 2, 3, 4.
  );
  if (pData == nullptr) {
    std::cerr << "Unable to load spritesheet image '"
      << spritesheetPath << '\'' << std::endl;
    return ERR_SPRITESHEET_LOAD_FAILED;
  }

  glActiveTexture(textureUnit);
  glBindTexture(GL_TEXTURE_2D, textureId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, *pImgWidth, *pImgHeight, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, pData);
  stbi_image_free(pData);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  if (checkGLErrors(__FILE__, __LINE__)) {
    return ERR_SPRITESHEET_LOAD_FAILED;
  }
  return 0;
}


int
main(int const argc, char const* const* argv) {
  // SDL.
  // According to the docs, SDL_Quit can be called even if SDL_Init fails.
  atexit(SDL_Quit);
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
    return ERR_SDL_INIT_FAILED;
  }

  // Window.
  constexpr Uint32 windowWidth = 640;
  constexpr Uint32 windowHeight = 480;
  std::unique_ptr<SDL_Window, SDLWindowDeleter> window{SDL_CreateWindow(
    "Spectre demo",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    windowWidth,
    windowHeight,
    SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
  )};
  if (!window) {
    return ERR_WINDOW_INIT_FAILED;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(
      SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  // OpenGL context.
  std::unique_ptr<void, SDLGLContextDeleter> glContext{
    SDL_GL_CreateContext(window.get())
  };
  if (!glContext) {
    return ERR_GL_CONTEXT_INIT_FAILED;
  }
  if (auto const error = glGetError(); error != GL_NO_ERROR) {
    std::cerr << "GL error: " << error << std::endl;
    return ERR_GL_ERROR_ON_CONTEXT_CREATION;
  }

  // GLEW initialisation.
  glewExperimental = GL_TRUE;
  if (GLenum const glewError = glewInit(); glewError != GLEW_OK) {
    std::cerr << "GLEW error: " << glewGetErrorString(glewError);
    return ERR_GLEW_INIT_FAILED;
  }

  // GL configuration.
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  // glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (checkGLErrors(__FILE__, __LINE__)) {
    return ERR_GL_CONFIG_FAILED;
  }

  // Load GL program.
  ogl::Program<ogl::ownership::Unique> program(ogl::g_initialised);
  int errorCode = 0;
  program.attach(
    [](std::string const& shaderLog, GLuint shaderId, GLenum type) {
      std::cout << "Shader log:" << std::endl << shaderLog << std::endl;
    },
    [&errorCode](GLuint shaderId, GLenum type) {
      char const* pTypeStr;
      switch (type) {
        case GL_VERTEX_SHADER:   pTypeStr = "vertex";   break;
        case GL_FRAGMENT_SHADER: pTypeStr = "fragment"; break;
        case GL_GEOMETRY_SHADER: pTypeStr = "geometry"; break;
        default: pTypeStr = "unknown";
      }
      std::cerr << "Failed to compile " << pTypeStr << " shader "
        << shaderId << std::endl;
      errorCode = ERR_GL_SHADER_COMPILATION_FAILED;
    },
    std::make_pair(GL_VERTEX_SHADER,   loadTextFile(VERT_SHADER_PATH)),
    std::make_pair(GL_FRAGMENT_SHADER, loadTextFile(FRAG_SHADER_PATH))
  );
  if (errorCode != 0) {
    return errorCode;
  }
  program.link(
    [](std::string const& programLog, GLuint programId) {
      std::cout << "Program log:" << std::endl << programLog << std::endl;
    },
    [&errorCode](auto&& ...) {
      std::cerr << "Failed to link program" << std::endl;
      errorCode = ERR_GL_PROGRAM_LINK_FAILED;
    }
  );
  if (errorCode != 0) {
    return errorCode;
  }
  program.use();
  if (checkGLErrors(__FILE__, __LINE__)) {
    return ERR_GL_PROGRAM_CREATION_FAILED;
  }
  ogl::uniform(program.getUniformLocation("mvp"), GL_FALSE,
               glm::ortho(-0.5f * windowWidth, 0.5f * windowWidth,
                          -0.5f * windowHeight, 0.5f * windowHeight));

  ogl::Vao<ogl::ownership::Unique> vao(ogl::g_initialised);
  vao.bind();

  // Initialise the spectre::Spritesheet. This contains a sprite atlas, which
  // is sent to the GPU using a texture buffer; and a 2D spritesheet texture.
  spectre::Spritesheet spritesheet(
    ogl::g_initialised, GL_TEXTURE0, GL_TEXTURE1);
  {
    // Load image.
    int imageWidth, imageHeight;
    errorCode = loadImage(
      spritesheet.imageTextureUnit(),
      spritesheet.imageTexture().id(),
      &imageWidth,
      &imageHeight);
    if (errorCode != 0) {
      std::cerr << "Error after loading spritesheet image" << std::endl;
      return errorCode;
    }

    // Generate frames.
    int const frameWidth = getIntegerEnvVar("framewidth", 32);
    int const frameHeight = getIntegerEnvVar("frameheight", 32);
    int frameCount = getIntegerEnvVar("framecount");
    if (frameCount == 0) {
      frameCount = (imageWidth / frameWidth) * (imageHeight / frameHeight);
    }
    auto const frames = spectre::createGridFrames(
      frameCount, imageWidth, imageHeight, frameWidth, frameHeight);
    spritesheet.assignFrames(frames.get(), frameCount);
  }

  // Set transform data.
  glm::vec2 scale{20.f, 20.f};
  using batch_type = spectre::Batch<Sprite, Sprite::Filter>;
  batch_type batch(ogl::g_initialised, GL_TEXTURE2);
  batch.assign({
    Sprite{
      glm::vec3{-20.f, -20.f, 0.4f},
      0,
      glm::quat{1.f, 0.f, 0.f, 0.f},
      scale,
    },
    Sprite{
      glm::vec3{20.f, -20.f, 0.4f},
      1,
      glm::quat{1.f, 0.f, 0.f, 0.f},
      scale,
    },
    Sprite{
      glm::vec3{20.f, 20.f, 0.4f},
      2,
      glm::quat{1.f, 0.f, 0.f, 0.f},
      scale,
    },
    Sprite{
      glm::vec3{-20.f, 20.f, 0.4f},
      3,
      glm::quat{1.f, 0.f, 0.f, 0.f},
      scale,
    },
  });
  if (checkGLErrors(__FILE__, __LINE__)) {
    std::cerr << "Error after buffering transform data" << std::endl;
    return ERR_TEX_BUFFER_ERROR;
  }

  ogl::uniform(program.getUniformLocation("spritesheet"),
               int(spritesheet.imageTextureUnit() - GL_TEXTURE0));
  ogl::uniform(program.getUniformLocation("frames"),
               int(spritesheet.bufferTextureUnit() - GL_TEXTURE0));
  ogl::uniform(program.getUniformLocation("actors"),
               int(batch.textureUnit() - GL_TEXTURE0));
  if (checkGLErrors(__FILE__, __LINE__)) {
    return ERR_UNIFORM_SET_ERROR;
  }

  glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

  bool keepRunning = getIntegerEnvVar("monoframe") == 0;
  SDL_Event event;
  do {
    { // Update sprite data.
      auto const ms = SDL_GetTicks();
      auto* const sprites = batch.data();

      // Update the animation frame of this sprite.
      sprites[0].setFrame((ms / 300) % 4);
      sprites[0].m_tween = (ms % 300) / 300.f;

      // Rotate this sprite.
      sprites[1].m_quat = glm::angleAxis(
        0.0005f * ms, glm::vec3(0.f, 0.f, 1.f));

      // Toggle the visibility of this sprite.
      sprites[2].m_thisFrameIdx = -((ms / 1200) % 2 != 0);

      // Scale this sprite.
      sprites[3].m_scale.x = 15.f * std::sin(0.001f * ms);
      sprites[3].m_scale.y = 15.f * std::cos(0.002f * ms);
      batch.update();
    }

    // Render.
    glClear(GL_COLOR_BUFFER_BIT);
    batch.render();
    SDL_GL_SwapWindow(window.get());

    while (SDL_PollEvent(&event) != 0) {
      if (event.type == SDL_QUIT || (
            event.type == SDL_KEYDOWN &&
            event.key.keysym.sym == SDLK_ESCAPE))
      {
        keepRunning = false;
        break;
      }
    }
  } while (keepRunning);

  return 0;
}
