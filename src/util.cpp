#include <cassert>

#include "spectre/util.h"


namespace spectre {


void
assignGridFrames(
  Frame* frames,
  int count,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight
) {
  assert((imgWidth % frameWidth == 0) && "frame must divide image width");
  assert((imgHeight % frameHeight == 0) && "frame must divide image height");
  int const numCols = imgWidth / frameWidth;
  int const numRows = imgHeight / frameHeight;
  assert((count <= numCols * numRows) && "frame count exceeds image size");
  float const fw = float(frameWidth) / imgWidth;
  float const fh = float(frameHeight) / imgHeight;
  for (int i = 0; i < count; ++i) {
    int const col = i % numCols;
    int const row = i / numCols;
    frames[i] = {
      float(col) / numCols + 0.5f * fw,
      float(row) / numRows + 0.5f * fh,
      fw,
      fh
    };
  }
}



void
assignGridFramesSubSection(
  Frame* frames,
  int count,
  int numColumns,
  int imgWidth,
  int imgHeight,
  int frameWidth,
  int frameHeight,
  int uOffset,
  int vOffset
) {
  // TODO: uOffset += hw etc.
  int const numRows = count / numColumns;
  assert((uOffset + numColumns < imgWidth ) && "Subsection too wide");
  assert((vOffset + numRows < imgHeight) && "Subsection too tall");
  int const hw = frameWidth / 2;
  int const hh = frameHeight / 2;
  float const uvFrameWidth  = float(frameWidth ) / imgWidth;
  float const uvFrameHeight = float(frameHeight) / imgHeight;
  for (int row = 0, frame = 0; row < numRows; ++row) {
    for (int col = 0; col < numColumns && frame < count; ++col, ++frame) {
      frames[frame] = {
        float(uOffset + col * frameWidth  + hw) / imgWidth,
        float(vOffset + row * frameHeight + hh) / imgHeight,
        uvFrameWidth,
        uvFrameHeight
      };
    }
  }
}


void
initTextureBuffer(
  GLuint textureId,
  GLenum textureUnit,
  GLuint bufferId,
  void const* data,
  std::size_t numBytes
) {
  glBindBuffer(GL_TEXTURE_BUFFER, bufferId);
  glBufferData(GL_TEXTURE_BUFFER, numBytes, data, GL_DYNAMIC_DRAW);
  glActiveTexture(textureUnit);
  glBindTexture(GL_TEXTURE_BUFFER, textureId);
  glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, bufferId);
}


} // namespace spectre
