# Build script, mostly for CI builds.
# Input environment variables:
# - CXX
# - CMAKE_BUILD_TYPE
# - BUILD_DIR (optional).
if [ -z "$CXX" ]; then
  echo "Error: CXX is not set"
  exit 1
fi
if [ -z "$CMAKE_BUILD_TYPE" ]; then
  echo "Error: CMAKE_BUILD_TYPE is not set"
  exit 2
fi
if [ -z "$BUILD_DIR" ]; then
  BUILD_DIR="`pwd`/build/$CXX/$CMAKE_BUILD_TYPE"
fi
set -u
set -e


echo "CMAKE_BUILD_TYPE: $CMAKE_BUILD_TYPE"
echo "BUILD_DIR: $BUILD_DIR"


(
  set -x
  mkdir -p "$BUILD_DIR"
  cmake -S . -B "$BUILD_DIR" -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE
  make --directory="$BUILD_DIR"
  cd "$BUILD_DIR"
  ctest --extra-verbose --progress
)
