#.rst:
# Findogl
# -------

# Find ogl dirs and libraries
# 
# Use this module by invoking find_package with the form::
#
#   find_package(ogl [version] [EXACT] [REQUIRED])
#
# This module finds headers and the ogl library, or a CMake package
# configuration file provided by an ogl CMake build. For the for
include(GNUInstallDirs)

if(NOT ogl_FOUND)
  if(NOT DEFINED ogl_INCLUDE_DIRS)
    find_path(ogl_INCLUDE_DIRS
      NAMES
        "ogl/Buffer.h"
        "ogl/Program.h"
        "ogl/Shader.h"
      PATHS
        "${ogl_ROOT}/include"
        "${CMAKE_INCLUDE_PATH}")
  endif()

  if(NOT DEFINED ogl_LIBRARIES)
    find_library(ogl_LIBRARIES NAMES ogl "${ogl_BUILD_DIR}")
  endif()
endif()


if(ogl_FOUND AND NOT TARGET ogl::ogl)
  message(STATUS "Target ogl::ogl does not exist; creating it")
  add_library(ogl::ogl INTERFACE IMPORTED)
  set_target_properties(ogl::ogl PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${ogl_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES ${GLEW_LIBRARIES} ${OPENGL_LIBRARIES})
endif()
